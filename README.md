﻿Challenge 1 : 

ticker	varchar(4)	Unchecked
date	varchar(12)	Unchecked
[open]	decimal(8, 4)	Unchecked
high	decimal(8, 4)	Unchecked
low	decimal(8, 4)	Unchecked
[close]	decimal(8, 4)	Unchecked
vol	int		Unchecked


CREATE PROCEDURE vwap
@start varchar(12)
AS 
SELECT ticker, SUM([close] * vol)/SUM(vol) as VWAP,
convert(varchar,convert(datetime,substring(@start,1,8)),103) AS DATE ,

'START: (' +substring(convert(varchar(12),convert(datetime,
substring(@start,1,8)+' '+
substring(@start,9,2)+ ':'+ substring(@start,11,2) ),108),1,5)+
') - END: (' + 
substring(convert(varchar(12), DATEADD(hh,+5, convert (datetime,
substring(@start,1,8)+' '+substring(@start,9,2)+':'+substring(@start,11,2))),108),1,5)+')' as Interval


FROM dataset 
WHERE convert(datetime,(substring([date],1,8)+' ' +
substring([date],9,2)+':'+substring([date],11,2)))  

BETWEEN convert(datetime,(substring(@start,1,8)+' ' +
substring(@start,9,2)+':'+substring(@start,11,2))) AND DATEADD(hh,+5,
convert(datetime,(substring(@start,1,8)+' ' +
substring(@start,9,2)+':'+substring(@start,11,2))))
GROUP by ticker
DROP PROCEDURE vwap

exec vwap '201010110900'

____________________________________________________________________________________________________________________________________________________________
		

Challenge 2:

Date	varchar(4)	Unchecked
Time	varchar(12)	Unchecked
[Open]	decimal(8, 4)	Unchecked
High	decimal(8, 4)	Unchecked
Low	decimal(8, 4)	Unchecked
[Close]	decimal(8, 4)	Unchecked
Volume	int		Unchecked

SELECT DISTINCT TOP 3 [Date], abs([open]-[close]) AS RANGE
INTO #temptable
FROM dataset2
ORDER BY RANGE DESC
SELECT TOP 3 [Date],[Range]
FROM #temptable
DROP TABLE #temptable



SELECT DISTINCT TOP 5 [Date] AS DATE, max([high]) AS MaxHigh 
INTO #temptable2 
FROM dataset2
WHERE [Date] IN (SELECT DATE FROM #temptable)
GROUP BY [Date]
ORDER BY MaxHigh DESC


 SELECT DISTINCT TOP 4 dataset2.[Date] AS DATE, dataset2.[Time] AS TIME 
 INTO #temptable3 
 FROM dataset2, #temptable2 
 WHERE dataset2.[high]= MaxHigh AND dataset2.[Date] = #temptable2.[date]
 GROUP BY dataset2.[Date],[Time]
 ORDER BY DATE ASC


 SELECT DISTINCT #temptable.[Date] AS DATE, RANGE, TIME 
 FROM #temptable3 LEFT JOIN #temptable ON #temptable3.[Date] = #temptable.[Date]

		